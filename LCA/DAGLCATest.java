import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class DAGLCATest {
	DAGLCA dag;
	DAGNode _0 = new DAGNode(0);
	DAGNode _1 = new DAGNode(1);
	DAGNode _2 = new DAGNode(2);
	DAGNode _3 = new DAGNode(3);
	DAGNode _4 = new DAGNode(4);
	DAGNode _5 = new DAGNode(5);
	DAGNode _6 = new DAGNode(6);
	
	@BeforeEach
	public void setUp() throws Exception {
		System.out.println("Before main test");
		dag = new DAGLCA(_0);
		List<DAGNode> l = new ArrayList<DAGNode>();
		l.add(_0);
		dag.addNode(_1, l);
		dag.addNode(_2, l);
		l.clear();
		l.add(_2);
		dag.addNode(_3, l);
		
		l.clear();
		l.add(_3);
		l.add(_1);
		dag.addNode(_4, l);
		
		l.clear();
		l.add(_2);
		l.add(_1);
		dag.addNode(_5, l);

	}

	@Test
	void test() {
		
		// test when there are multiple solutions
		List<DAGNode> solutions = dag.findLCA(_4, _5);
		assertTrue(solutions.contains(_1));
		assertTrue(solutions.contains(_2));
		// testing that swapping the arguments has no effect and the solutions are the same
		solutions = dag.findLCA(_5, _4);
		assertTrue(solutions.contains(_1));
		assertTrue(solutions.contains(_2));
		// testing with a node that is node in the graph
		solutions = dag.findLCA(_5, _6);
		assertTrue(solutions.isEmpty());
		solutions = dag.findLCA(_6, _5);
		assertTrue(solutions.isEmpty());
		// testing with the root as both arguments
		solutions = dag.findLCA(_0, _0);
		assertTrue(solutions.contains(_0));
		// testing with the root and one of its children
		solutions = dag.findLCA(_1, _0);
		assertTrue(solutions.contains(_0));
		// testing with a leaf as both arguments
		solutions = dag.findLCA(_5, _5);
		assertTrue(solutions.contains(_5));
		// testing with a random node as both arguments
		solutions = dag.findLCA(_2, _2);
		assertTrue(solutions.contains(_2));
	}

}
