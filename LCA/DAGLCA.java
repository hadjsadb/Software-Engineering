import java.util.ArrayList;
import java.util.List;

enum Colour {WHITE, BLUE, RED}

class DAGNode {
	
	
	int data;
	ArrayList<DAGNode> parents;
	ArrayList<DAGNode> children;
	
	Colour colour;
	int count;
	
	// constructor
	DAGNode(int value) {
		data = value;
		parents = new ArrayList<DAGNode>();
		children = new ArrayList<DAGNode>();
		colour = Colour.WHITE;
		count = 0;
	}
	
	
	public String toString() {
		return "" + data;
	}
}




public class DAGLCA {
	
	public DAGNode root;
	
	public DAGLCA(DAGNode root) {
		this.root = root;
	}
	
	public void addNode(DAGNode node, List<DAGNode> parents) {
		for (DAGNode parent : parents) {
			if (parent == root) {
				System.out.println("Root is connected");
			}
			parent.children.add(node);
			node.parents.add(parent);
		}
	}
	
	/*public void colour(DAGNode n, Colour colour) {
		if (n == null)
			return;
		for (DAGNode parent : n.parents) {
			if (parent.colour == Colour.WHITE && colour == Colour.BLUE) {
				parent.colour = colour;
			}
			else if (parent.colour == Colour.BLUE && colour == Colour.RED) {
				for (DAGNode parentParent : parent.parents)
					parentParent.count++;
				parent.colour = colour;
			}
			colour(parent, colour);
		}
	}*/
	
	public void colour(DAGNode n, Colour colour) {
		if (n == null)
			return;
		
		if (n.colour == Colour.WHITE && colour == Colour.BLUE) {
			n.colour = colour;
		}
		else if (n.colour == Colour.BLUE && colour == Colour.RED) {
			for (DAGNode parent : n.parents)
				parent.count++;
			n.colour = colour;
		}
		
		for (DAGNode parent : n.parents) {
			colour(parent, colour);
		}
	}
	
	public void solutions(DAGNode n1, List<DAGNode> solutions) {
		
		if (n1 == null)
			return;
		if (n1.colour == Colour.RED && n1.count == 0)
			solutions.add(n1);
		for (DAGNode parent : n1.parents) {
			solutions(parent, solutions);
		}
	}
	
	
	public void reset(DAGNode n) {
		if (n == null)
			return;
		n.colour = Colour.WHITE;
		n.count = 0;
		for (DAGNode parent : n.parents) {
			reset(parent);
		}
	}
	
	public List<DAGNode> findLCA(DAGNode n1, DAGNode n2) {
		List<DAGNode> solutions = new ArrayList<DAGNode>();
		if (n1 == n2) {
			solutions.add(n1);
		}
		else {	
			reset(n1);
			reset(n2);
			colour(n1, Colour.BLUE);
			colour(n2, Colour.RED);
			solutions(n1, solutions);
		}
		return solutions;
	}
	

	public static void main(String[] args) {
		DAGNode _0 = new DAGNode(0);
		DAGNode _1 = new DAGNode(1);
		DAGNode _2 = new DAGNode(2);
		DAGNode _3 = new DAGNode(3);
		DAGNode _4 = new DAGNode(4);
		DAGNode _5 = new DAGNode(5);
		DAGNode _6 = new DAGNode(6);
		DAGLCA dag = new DAGLCA(_0);
		
		List<DAGNode> l = new ArrayList<DAGNode>();
		l.add(_0);
		dag.addNode(_1, l);
		dag.addNode(_2, l);
		l.clear();
		l.add(_2);
		dag.addNode(_3, l);
		
		l.clear();
		l.add(_3);
		l.add(_1);
		dag.addNode(_4, l);
		
		l.clear();
		l.add(_2);
		l.add(_1);
		dag.addNode(_5, l);
		
		
		for (DAGNode node : dag.findLCA(_4, _5))
			System.out.println(node);
		
		for (DAGNode node : dag.findLCA(_5, _4))
			System.out.println(node);
		
		for (DAGNode node : dag.findLCA(_5, _6))
			System.out.println(node);
		
		for (DAGNode node : dag.findLCA(_6, _5))
			System.out.println(node);
		
		for (DAGNode node : dag.findLCA(_0, _0))
			System.out.println(node);
		
		for (DAGNode node : dag.findLCA(_1, _0))
			System.out.println(node);
	}

}
