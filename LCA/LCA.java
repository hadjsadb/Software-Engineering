
import java.util.ArrayList;
import java.util.List;

class Node {
	int data;
	Node left, right;

	// constructor
	Node(int value) {
		data = value;
		left = right = null;
	}
}

public class LCA {

	Node root;


	public Node addNode(Node parent, String side, int value) {
		Node newNode = new Node(value);
		if (side == "right" || side == "r" || side == "Right") {
			parent.right = newNode;
		} else if (side == "left" || side == "Left" || side == "l") {
			parent.left = newNode;
		} else {
			System.err.println("Please choose a correct side");
		}

		return parent;
	}

	public int findLCA(int n1, int n2) {
		return findLCAInternal(root, n1, n2);
	}

	private int findLCAInternal(Node root, int n1, int n2) {
		List<Integer> path1 = new ArrayList<>();
		List<Integer> path2 = new ArrayList<>();
		if (!findPath(root, n1, path1) || !findPath(root, n2, path2)) {
			System.out.println((path1.size() > 0) ? "n1 is present" + n1 : "n1 is missing" + n1);
			System.out.println((path2.size() > 0) ? "n2 is present" + n2 : "n2 is missing" + n2);
			return -1;
		}

		int i;
		for (i = 0; i < path1.size() && i < path2.size(); i++) {
			// System.out.println(path1.get(i) + " " + path2.get(i));
			if (!path1.get(i).equals(path2.get(i)))
				break;
		}

		return path1.get(i - 1);
	}

	private boolean findPath(Node root, int n, List<Integer> path) {
		if (root == null) {
			return false;
		}

		path.add(root.data);

		if (root.data == n) {
			return true;
		}

		if (root.left != null && findPath(root.left, n, path)) {
			return true;
		}

		if (root.right != null && findPath(root.right, n, path)) {
			return true;
		}

		path.remove(path.size() - 1);

		return false;
	}

	// 1
	// 2 3
	// 4 5 6 7
	// r = 1
	// n1 = 4 1-2-4
	// n2 = 5 1-2-5

	/*
	 * public static void main(String[] args) { LCA tree = new LCA(); tree.root =
	 * new Node(1); tree.root.left = new Node(2); tree.root.right = new Node(3);
	 * tree.root.left.left = new Node(4); tree.root.left.right = new Node(5);
	 * tree.root.right.left = new Node(6); tree.root.right.right = new Node(7);
	 * 
	 * tree.addNode(tree.root.right.left, "right", 8);
	 * tree.addNode(tree.root.right.left.right, "left", 9);
	 * 
	 * System.out.println("LCA(4, 5): " + tree.findLCA(4,5));
	 * System.out.println("LCA(4, 6): " + tree.findLCA(4,6));
	 * System.out.println("LCA(3, 4): " + tree.findLCA(3,4));
	 * System.out.println("LCA(2, 9): " + tree.findLCA(2,9)); }
	 */
}
