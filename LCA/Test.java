import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;

class Test {
	LCA tree;

	@BeforeEach
	public void setUp() throws Exception {
		System.out.println("Before main test");

		tree = new LCA();
		tree.root = new Node(1);
		tree.root.left = new Node(2);
		tree.root.right = new Node(3);
		tree.root.left.left = new Node(4);
		tree.root.left.right = new Node(5);
		tree.root.right.left = new Node(6);
		tree.root.right.right = new Node(7);

	}


	@org.junit.jupiter.api.Test
	void testFindLCA() {
		System.out.println(" main test");
		// System.out.println(tree.findLCA(20, 30));
		assertEquals(tree.getClass().toString(), "class LCA");
		// positive cases
		assertEquals(tree.findLCA(4, 5), 2);
		assertEquals(tree.findLCA(1, 3), 1);
		assertEquals(tree.findLCA(3, 1), 1);
		assertEquals(tree.findLCA(3, 2), 1);
		assertEquals(tree.findLCA(6, 7), 3);
		assertEquals(tree.findLCA(2, 7), 1);
		assertEquals(tree.findLCA(3, 7), 3);
		
		// test that the LCA of the root and itself is itself
		assertEquals(tree.findLCA(1, 1), 1);
		// test that the LCA of a leaf and itself is itself
		assertEquals(tree.findLCA(7, 7), 7);
		// test that the LCA of any other node and itself is itself 
		assertEquals(tree.findLCA(3, 3), 3);
		
		
		// test with invalid input
		assertEquals(tree.findLCA(4, -1), -1);
		assertEquals(tree.findLCA(-1, 4), -1);
		assertEquals(tree.findLCA(0, -1), -1);
		assertEquals(tree.findLCA(20, 30), -1);
		assertEquals(tree.findLCA(-20, 40), -1);
		assertEquals(tree.findLCA(-20, 5), -1);
		assertEquals(tree.findLCA(5, -5), -1);
		assertEquals(tree.findLCA(20, -20), -1);
	}

	/*
	 * @org.junit.jupiter.api.Test void testFindPath() {
	 * System.out.println("find path test"); List<Integer> path1 = new
	 * ArrayList<>(); assertEquals(tree.findPath(tree.root.left, 0,path1), false); }
	 */

	@org.junit.jupiter.api.Test
	void testAddNode() {
		System.out.println("add node test");
		assertEquals(tree.getClass().toString(), "class LCA");
		assertEquals(tree.addNode(tree.root.right, "right", -1), tree.root.right);
		assertEquals(tree.addNode(tree.root.right.left, "right", 0), tree.root.right.left);
		assertEquals(tree.addNode(tree.root.right.left, "left", 30), tree.root.right.left);
		assertEquals(tree.addNode(tree.root, "left", 30), tree.root);
		assertEquals(tree.addNode(tree.root, "right", 30), tree.root);
		// assertEquals(tree.addNode(tree.root.left.right, "ledt", 3), exit(1);
	}
};