from github3 import login

gh = login('testaccount654321', password='qwerty123456')
testaccount654321 = gh.user()
print(testaccount654321.to_json())


elasticsearch = gh.repository("GlenRSmith", "elasticsearch")


commit_counts_per_user = {}
count = 0
for commit in elasticsearch.iter_commits():

    if count % 100 == 0:
        print(count)
    count += 1
    email = commit.to_json()['commit']['author']['email']
    name = commit.to_json()['commit']['author']['name']
    if email in commit_counts_per_user:
        commit_counts_per_user[email][0] += 1
    else:
        commit_counts_per_user[email] = [1, name]

print(len(commit_counts_per_user))
print(commit_counts_per_user)

file = open("commit_counts_per_user.csv", "w")

file.write("id,value\n")
for email, value in commit_counts_per_user.items():
    try:
        file.write(value[1] + "," + str(value[0]) + "\n")
    except UnicodeEncodeError:
        pass
